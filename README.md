# Functional Programming Examples 📘

Welcome to the repository of functional programming examples created to demonstrate various
functional programming concepts and patterns in Java. 🚀

These examples are part of the educational materials for the **Functional Programming** seminar
conducted on **November 10, 2023**, for the LHV IT Development department. 👨‍💻

## Prerequisites 📋

- Java JDK 21 or higher 🛠️
- IntelliJ IDEA (optional) 💻

## Materials 📚

The slides used at the seminar are available [here](slides/Functional%20programming.pdf).

### Repository Structure 🏗️

- [**Concept of Pure Functions**](src/main/java/ee/lhv/fpexamples/purity) 🧼
  - `ImpureFunctionExample`: Illustrates the characteristics of impure functions.
  - `PureFunctionExample`: Demonstrates pure function implementation.
- [**Higher-order Functions**](src/main/java/ee/lhv/fpexamples/higherorder) 🔼
  - `CallbackExample`: Uses callbacks for asynchronous operations.
  - `FactoryPatternExample`: Applies the factory pattern using higher-order functions.
  - `MiddlewareExample`: Demonstrates middleware pattern while applying functions.
  - `StrategyPatternExample`: Implements the strategy pattern using functional interfaces.
- [**Function Composition**](src/main/java/ee/lhv/fpexamples/composition) 🔄
  - `CompositionPipeExample`: Implements function composition and piping.
- [**Closure**](src/main/java/ee/lhv/fpexamples/closure) 🔐
  - `ClosureExample`: Shows how closures capture the lexical environment.
  - `CurryingExample`: Provides a currying pattern to show argument transformations.
- [**Lazy Evaluation**](src/main/java/ee/lhv/fpexamples/laziness) 💤
  - `LazinessExample`: Illustrates lazy behavior of computations.
  - `MemoizationExample`: Implements memoization to cache function results.
  - `OptionalExample`: Uses the Optional class to avoid null checks.
- [**Pattern Matching and Abstract Data Types (ADT)**](src/main/java/ee/lhv/fpexamples/matching) 🔍
  - `MatchingExample`: Provides an example of pattern matching.
- [**Error and State Management: Railway-Oriented Programming**](src/main/java/ee/lhv/fpexamples/rop) 🛤️
  - `RopExample`: Introduces the basic concept of railway-oriented programming.
  - `Result`: Showcases an example wrapper for handling results and errors.
  - `UpdateEmailFunctionalUseCase`: Applies ROP principles to a practical use-case.