package ee.lhv.fpexamples.higherorder;

import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;

@RequiredArgsConstructor
public class CallbackExample {
    private final CalculationRepository calculationRepository;
    private final Calculator calculator;

    record Context(Double number, String data, Double otherNumber) {}

    private void handleCalculation(Context context) {
        calculator.performCalculation(context, ctx -> {
            System.out.println("Calculation finished!");
            calculationRepository.saveCalculation(ctx);
        });
    }

    interface CalculationRepository {
        void saveCalculation(Context ctx);
    }

    interface Calculator {
        void performCalculation(Context ctx, Consumer<Context> callback);
    }
}
