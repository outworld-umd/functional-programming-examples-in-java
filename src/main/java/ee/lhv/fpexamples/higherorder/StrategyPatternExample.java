package ee.lhv.fpexamples.higherorder;

import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.function.Consumer;

import static org.mockito.Mockito.mock;

@RequiredArgsConstructor
public class StrategyPatternExample {
    private final LegacyEventService legacyEventService = mock(LegacyEventService.class);
    private final NewEventService newEventService = mock(NewEventService.class);

    private final Map<EventType, Consumer<Event>> eventHandlers = Map.of(
            EventType.INSERT, legacyEventService::handleInsert,
            EventType.UPDATE, event -> legacyEventService.handleUpdate(event, 2),
            EventType.DELETE, newEventService::handleDelete
    );

    public void dispatch(Event event) {
        eventHandlers.getOrDefault(event.type(), e -> {
            System.out.println(STR."Unknown event: \{e}");
        }).accept(event);
    }

    enum EventType {
        INSERT, UPDATE, DELETE
    }

    public record Event(EventType type, String data) {}

    interface LegacyEventService {
        void handleInsert(Event event);
        void handleUpdate(Event event, int someParameter);
    }

    interface NewEventService {
        void handleDelete(Event event);
    }
}