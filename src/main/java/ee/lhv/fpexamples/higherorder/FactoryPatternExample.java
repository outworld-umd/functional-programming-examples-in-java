package ee.lhv.fpexamples.higherorder;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

public class FactoryPatternExample {

    private static final Map<Fruit.Type, Supplier<Fruit>> FACTORY_MAP = Map.of(
            Fruit.Type.APPLE, () -> new Fruit.Apple(5.5, "Green"),
            Fruit.Type.BANANA, () -> new Fruit.Banana(20),
            Fruit.Type.ORANGE, () -> new Fruit.Orange(6.7),
            Fruit.Type.PEAR, Fruit.Pear::new
    );

    private static Fruit create(Fruit.Type fruitType) {
        var supplier = Optional.of(fruitType).map(FACTORY_MAP::get).orElseThrow();
        return supplier.get();
    }

    public static void main(String[] args) {
        var apple = create(Fruit.Type.APPLE);
        System.out.println(apple.getClass().getSimpleName()); // Apple

        var banana = create(Fruit.Type.BANANA);
        System.out.println(banana.getClass().getSimpleName()); // Banana
    }

    interface Fruit {
        record Apple(double radius, String color) implements Fruit {}
        record Banana(double length) implements Fruit {}
        record Orange(double radius) implements Fruit {}
        record Pear() implements Fruit {}

        enum Type {
            APPLE, BANANA, ORANGE, PEAR
        }
    }
}
