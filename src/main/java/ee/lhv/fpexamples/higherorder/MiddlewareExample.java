package ee.lhv.fpexamples.higherorder;

import java.util.function.Function;
import java.util.stream.Stream;

public class MiddlewareExample {

    private static <I, O> Function<I, O> logging(Function<I, O> function) {
        return input -> {
            var output = function.apply(input);
            System.out.println(STR."Input was \{input}, output was: \{output}");
            return output;
        };
    }

    public static void main(String[] args) {
        var result = Stream.of(1, 2, 3, 4).map(logging(num -> num + 2)).toList();
        System.out.println(result);
    }
}