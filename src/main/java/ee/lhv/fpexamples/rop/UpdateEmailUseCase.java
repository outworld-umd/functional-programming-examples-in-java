package ee.lhv.fpexamples.rop;

import ee.lhv.fpexamples.rop.domain.Account;

public interface UpdateEmailUseCase extends UseCase<String, Account, UpdateEmailUseCase.FailureReason> {

    record FailureReason(Type type, Exception exception) {
        public enum Type {
            REQUEST_READ_FAILED,
            ACCOUNT_NOT_FOUND,
            VALIDATION_FAILED,
            NOTIFICATION_NOT_SENT,
        }

        static FailureReason of(Type type) {
            return new FailureReason(type, null);
        }
    }
}
