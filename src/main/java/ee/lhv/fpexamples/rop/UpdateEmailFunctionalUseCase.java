package ee.lhv.fpexamples.rop;

import ee.lhv.fpexamples.rop.domain.Account;
import ee.lhv.fpexamples.rop.domain.AccountMapper;
import ee.lhv.fpexamples.rop.domain.AccountRepository;
import ee.lhv.fpexamples.rop.util.DataReader;
import ee.lhv.fpexamples.rop.util.EmailHelper;
import ee.lhv.fpexamples.rop.util.NotificationService;
import ee.lhv.fpexamples.rop.util.Pair;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Stream;

import static ee.lhv.fpexamples.rop.Result.attempt;
import static ee.lhv.fpexamples.rop.Result.ensure;
import static ee.lhv.fpexamples.rop.Result.onFailure;
import static ee.lhv.fpexamples.rop.Result.onFailureDo;
import static ee.lhv.fpexamples.rop.Result.onSuccess;
import static ee.lhv.fpexamples.rop.Result.onSuccessDo;
import static ee.lhv.fpexamples.rop.Result.optional;
import static ee.lhv.fpexamples.rop.Result.tryTo;

@Log
@RequiredArgsConstructor
public final class UpdateEmailFunctionalUseCase implements UpdateEmailUseCase {
    private final DataReader dataReader;
    private final AccountMapper accountMapper;
    private final EmailHelper emailHelper;
    private final NotificationService notificationService;
    private final AccountRepository accountRepository;

    public Result<Account, FailureReason> execute(String request) {
        return Result.<String, FailureReason>of(request)
                .then(tryTo(dataReader::<EmailChangeRequest>readValue, ex -> new FailureReason(FailureReason.Type.REQUEST_READ_FAILED, ex)))
                .then(attempt(this::validateRequest))
                .then(onSuccess(emailHelper::canonicalize))
                .then(attempt(this::findRequestAccount))
                .then(onSuccess(bundle -> accountMapper.map(bundle.left(), bundle.right().email())))
                .then(onSuccessDo(accountRepository::update))
                .then(attempt(account -> sendMessage(account).then(onSuccess(_ -> account))));
    }

    private Result<Pair<Account, EmailChangeRequest>, FailureReason> findRequestAccount(EmailChangeRequest request) {
        return optional((EmailChangeRequest req) -> Optional.of(req)
                        .map(EmailChangeRequest::id)
                        .map(accountRepository::get)
                        .map(account -> new Pair<>(account, request)),
                () -> FailureReason.of(FailureReason.Type.ACCOUNT_NOT_FOUND)
        ).apply(request);
    }

    private Result<EmailChangeRequest, FailureReason> validateRequest(EmailChangeRequest request) {
        return Validator.validate(request, Stream.of(
                Validator.acceptIf(req -> Objects.nonNull(req.id()), "Id is missing!"),
                Validator.acceptIf(req -> Objects.nonNull(req.email()), "Email is missing!"),
                Validator.on(EmailChangeRequest::email, Stream.of(
                        Validator.rejectIf(email -> email.length() < 5, "Email too short!"),
                        Validator.rejectIf(email -> email.contains("õ"), "Email contains letter banned on Saaremaa!"))
                ))
        ).then(onFailure(ex -> new FailureReason(FailureReason.Type.VALIDATION_FAILED, ex)));
    }

    private Result<NotificationService.Message, FailureReason> sendMessage(Account account) {
        return Result.<Account, FailureReason>of(account)
                .then(onSuccess(this::createNotificationMessage))
                .then(ensure(notificationService::sendMessage, () -> FailureReason.of(FailureReason.Type.NOTIFICATION_NOT_SENT)))
                .then(onFailureDo(reason -> log.log(Level.ALL, reason.toString())));
    }

    private NotificationService.Message createNotificationMessage(Account account) {
        return new NotificationService.Message(account.email(), "Account was updated!");
    }

    public record EmailChangeRequest(Integer id, String email) {
    }
}
