package ee.lhv.fpexamples.rop.util;

import java.text.ParseException;

public interface DataReader {
    <T> T readValue(String data) throws ParseException;
}