package ee.lhv.fpexamples.rop.util;

public interface NotificationService {
    boolean sendMessage(Message message);

    record Message(String email, String content) {
    }
}