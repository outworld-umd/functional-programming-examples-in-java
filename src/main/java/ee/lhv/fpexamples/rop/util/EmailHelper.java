package ee.lhv.fpexamples.rop.util;

import ee.lhv.fpexamples.rop.UpdateEmailFunctionalUseCase;

public interface EmailHelper {
    UpdateEmailFunctionalUseCase.EmailChangeRequest canonicalize(UpdateEmailFunctionalUseCase.EmailChangeRequest email);
}