package ee.lhv.fpexamples.rop;

import ee.lhv.fpexamples.rop.functions.FunctionHelper;
import ee.lhv.fpexamples.rop.functions.ThrowingFunction;

import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public sealed interface Result<S, F> {

    /**
     * Applies one of two given functions to the value held by this result, depending on whether this is a success or a failure.
     * @param onSuccess Function to apply if this is a success.
     * @param onFailure Function to apply if this is a failure.
     * @return Result of applying the appropriate function.
     */
    default <R> R either(Function<S, R> onSuccess, Function<F, R> onFailure) {
        return switch (this) {
            case Result.Success<S, F>(S value) -> onSuccess.apply(value);
            case Result.Failure<S, F>(F value) -> onFailure.apply(value);
        };
    }

    /**
     * Applies a function to this result.
     * @param function The function to apply to this result.
     * @return Result of applying the function.
     */
    default <R> R then(Function<Result<S, F>, R> function) {
        return function.apply(this);
    }

    record Success<S, F>(S value) implements Result<S, F> {
    }

    record Failure<S, F>(F value) implements Result<S, F> {
    }

    /**
     * @see Result#success(S value)
     */
    static <S, F> Result<S, F> of(S value) {
        return Result.success(value);
    }

    /**
     * Creates a Success instance containing the given value.
     * @param value The success value.
     * @return A success result.
     */
    static <S, F> Result<S, F> success(S value) {
        return new Success<>(value);
    }

    /**
     * Creates a Failure instance containing the given value.
     * @param value The failure value.
     * @return A failure result.
     */
    static <S, F> Result<S, F> failure(F value) {
        return new Failure<>(value);
    }

    /**
     * Attempts to apply a given function to a success value, or returns the failure.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     * ====S====|====X=====|====S1====
     *          |    \\    |
     *          |     \\   |
     * ====F====|==========|=====F====
     *          |__________|
     * </pre>
     * @param f The function to apply.
     * @return A function that attempts to apply the given function or returns failure.
     */
    static <S, S1, F> Function<Result<S, F>, Result<S1, F>> attempt(Function<S, Result<S1, F>> f) {
        return r -> r.either(
                f,
                Result::failure
        );
    }

    /**
     * Transforms the success value using the provided function, or passes through failure.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     * ====S====|====X=====|====S1====
     *          |          |
     * ====F====|==========|=====F====
     *          |__________|
     * </pre>
     * @param f The function to apply to the success value.
     * @return A function that transforms the success value or returns failure.
     */
    static <S, S1, F> Function<Result<S, F>, Result<S1, F>> onSuccess(Function<S, S1> f) {
        return r -> r.either(
                f.andThen(Result::success),
                Result::failure
        );
    }

    /**
     * Transforms the failure value using the provided function, or passes through success.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     * ====S====|==========|=====S====
     *          |          |
     * ====F====|====X=====|====F1====
     *          |__________|
     * </pre>
     * @param f The function to apply to the failure value.
     * @return A function that transforms the failure value or returns success.
     */
    static <S, F, F1> Function<Result<S, F>, Result<S, F1>> onFailure(Function<F, F1> f) {
        return r -> r.either(
                Result::success,
                f.andThen(Result::failure)
        );
    }

    /**
     * Executes a callback on the success value.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     *          |    //    |
     * ====S====|====X=====|=====S====
     *          |          |
     * ====F====|==========|=====F====
     *          |__________|
     * </pre>
     * @param f The consumer to execute.
     * @return A function that executes the consumer on the success value or returns failure.
     */
    static <S, F> Function<Result<S, F>, Result<S, F>> onSuccessDo(Consumer<S> f) {
        return onSuccess(FunctionHelper.peek(f));
    }

    /**
     * Executes a callback on the failure value.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     * ====S====|==========|=====S====
     *          |          |
     * ====F====|====X=====|=====F====
     *          |    \\    |
     *          |__________|
     * </pre>
     * @param f The consumer to execute.
     * @return A function that executes the consumer on the failure value or returns success.
     */
    static <S, F> Function<Result<S, F>, Result<S, F>> onFailureDo(Consumer<F> f) {
        return onFailure(FunctionHelper.peek(f));
    }

    /**
     * Returns the success value or transforms the failure value into success.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     * ====S====|==========|=====S====
     *          |     //   |
     *          |    //    |
     * ====F====|====X     |
     *          |__________|
     * </pre>
     * @param f The function to transform the failure value if present.
     * @return A function that returns the success value or transforms the failure.
     */
    static <S, F> Function<Result<S, F>, S> otherwise(Function<F, S> f) {
        return r -> r.either(
                Function.identity(),
                f
        );
    }

    /**
     * Returns the success value or attempts to recover the failure value into success.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾|
     * ====S====|==========|=====S====
     *          |     //   |
     *          |    //    |
     * ====F====|====X=====|=====F====
     *          |__________|
     * </pre>
     * @param f The function to transform the failure value if present.
     * @return A function that returns the result.
     */
    static <S, F> Function<Result<S, F>, Result<S, F>> recover(Function<F, Result<S, F>> f) {
        return r -> r.either(
                Result::success,
                f
        );
    }

    /**
     * Merges two Result instances, prioritizing successes and combining failures.
     * <pre>
     *
     * +---------------+---------------+---------------+
     * |     Result    |  Success (S2) |  Failure (F2) |
     * +---------------+---------------+---------------+
     * |  Success (S1) |       S1      |       F2      |
     * +-------------------------------+---------------+
     * |  Failure (F1) |       F1      |    F1 & F2    |
     * +-------------------------------+---------------+
     * </pre>
     * @param f The binary operator to combine two failure values.
     * @return A binary operator that merges two Result instances.
     */
    static <S, F> BinaryOperator<Result<S, F>> merge(BinaryOperator<F> f) {
        return (result1, result2) -> result2.either(
                success2 -> result1,
                failure2 -> result1.either(
                        success1 -> result2,
                        failure1 -> Result.failure(f.apply(failure1, failure2))
                ));
    }

    /**
     * Ensures the success value meets the given predicate, or returns a failure with the provided value.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾‾‾‾|
     * ====S====|====X==true==|=====S====
     *          |    \\       |
     *          |   false     |
     *          |      \\     |
     * ====F====|=============|=====F====
     *          |_____________|
     * </pre>
     * @param test The predicate the success value must satisfy.
     * @param failure The failure value supplier if the predicate is not satisfied.
     * @return A function that ensures the predicate is satisfied or returns failure.
     */
    static <S, F> Function<Result<S, F>, Result<S, F>> ensure(Predicate<S> test, Supplier<F> failure) {
        return attempt(s -> test.test(s) ? Result.success(s) : Result.failure(failure.get()));
    }

    /**
     * Maps an optional result from a function, providing a failure if the result is empty.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾‾‾‾|
     * ====S====|====X====OK==|====S1====
     *          |    \\       |
     *          |   empty     |
     *          |      \\     |
     * ====F====|=============|=====F====
     *          |_____________|
     * </pre>
     * @param f The function that returns an Optional value.
     * @param ifMissing The supplier that provides the failure value if the Optional is empty.
     * @return A function that maps the Optional result or provides a failure.
     */
    static <S, S1, F> Function<S, Result<S1, F>> optional(Function<S, Optional<S1>> f, Supplier<F> ifMissing) {
        return f.andThen(opt -> opt
                .map(Result::<S1, F>success)
                .orElseGet(() -> Result.failure(ifMissing.get())));
    }

    /**
     * Attempts to apply a throwing function to a success value, capturing exceptions as failure values.
     * <pre>
     *
     *          |‾‾‾‾‾‾‾‾‾‾‾‾‾|
     * ====S====|====X====OK==|====S1====
     *          |    \\       |
     *          |  exception  |
     *          |      \\     |
     * ====F====|=============|=====F====
     *          |_____________|
     * </pre>
     * @param f The throwing function to apply.
     * @param onException The function to transform an exception into a failure value.
     * @return A function that attempts to apply the throwing function or returns a failure.
     */
    static <S, S1, F, X extends Exception> Function<Result<S, F>, Result<S1, F>> tryTo(
            ThrowingFunction<S, S1, X> f,
            Function<Exception, F> onException
    ) {
        return attempt(s -> {
            try {
                return Result.success(f.apply(s));
            } catch (Exception ex) {
                return Result.failure(onException.apply(ex));
            }
        });
    }
}
