package ee.lhv.fpexamples.rop;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static java.util.function.Predicate.not;

public interface Validator<S, F> extends Function<S, Result<S, Stream<F>>> {

    static <S, T, F> Validator<S, F> on(Function<S, T> f, Stream<Validator<T, F>> validators) {
        return value -> f
                .andThen(g -> doValidate(g, validators))
                .andThen(Result.onSuccess(h -> value))
                .apply(value);
    }

    static <S, F> Validator<S, F> rejectIf(Predicate<S> predicate, F reason) {
        return acceptIf(not(predicate), reason);
    }

    static <S, F> Validator<S, F> acceptIf(Predicate<S> predicate, F reason) {
        return value -> predicate.test(value) ? Result.success(value) : Result.failure(Stream.of(reason));
    }

    static <S, F> Result<S, Stream<F>> doValidate(S value, Stream<Validator<S, F>> validators) {
        return validators
                .map(fn -> Optional.ofNullable(value).map(fn).orElseGet(() -> Result.success(value)))
                .reduce(Result.success(value), Result.merge(Stream::concat));
    }

    static <S, F> Result<S, ValidationException> validate(S value, Stream<Validator<S, F>> validators) {
        return doValidate(value, validators)
                .then(Result.onFailure(Stream::toList))
                .then(Result.onFailure(ValidationException::with));
    }

    @ToString
    @RequiredArgsConstructor(staticName = "with")
    class ValidationException extends RuntimeException {
        private final transient Collection<?> violations;
    }
}
