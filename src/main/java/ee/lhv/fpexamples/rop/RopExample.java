package ee.lhv.fpexamples.rop;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;

import static ee.lhv.fpexamples.rop.Result.onSuccess;
import static ee.lhv.fpexamples.rop.Result.otherwise;

@Log
@RequiredArgsConstructor
public class RopExample {
    private final UpdateEmailUseCase updateEmailUseCase;

    public EmailChangeResponse updateEmail(String request) {
        return updateEmailUseCase.execute(request)
                .then(onSuccess(result -> new EmailChangeResponse(200, "OK", "Email has been updated!")))
                .then(otherwise(RopExample::handleFailure));
    }

    private static EmailChangeResponse handleFailure(UpdateEmailUseCase.FailureReason reason) {
        return switch (reason.type()) {
            case REQUEST_READ_FAILED -> new EmailChangeResponse(400, "Incorrect request data!", reason.exception().getMessage());
            case ACCOUNT_NOT_FOUND -> new EmailChangeResponse(404, "No account!", null);
            case VALIDATION_FAILED -> new EmailChangeResponse(400, "Invalid request params!", null);
            case NOTIFICATION_NOT_SENT -> new EmailChangeResponse(500, "Something on our side! Whoopsie!", reason.exception().getMessage());
        };
    }

    public record EmailChangeResponse(Integer status, String code, String message) {
    }
}
