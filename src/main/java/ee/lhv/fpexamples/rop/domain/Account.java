package ee.lhv.fpexamples.rop.domain;

public record Account(Integer id, String email) {
}
