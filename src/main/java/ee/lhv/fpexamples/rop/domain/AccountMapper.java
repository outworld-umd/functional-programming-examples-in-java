package ee.lhv.fpexamples.rop.domain;

public interface AccountMapper {
    Account map(Account account, String email);
}