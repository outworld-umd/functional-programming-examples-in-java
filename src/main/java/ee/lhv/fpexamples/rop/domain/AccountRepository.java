package ee.lhv.fpexamples.rop.domain;

public interface AccountRepository {
    Account get(Integer id);

    void update(Account account);
}