package ee.lhv.fpexamples.rop.functions;

@FunctionalInterface
public interface ThrowingFunction<T, R, X extends Exception> {
    R apply(T value) throws X;
}
