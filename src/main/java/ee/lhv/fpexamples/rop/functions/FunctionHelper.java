package ee.lhv.fpexamples.rop.functions;

import lombok.experimental.UtilityClass;

import java.util.function.Consumer;
import java.util.function.Function;

@UtilityClass
public final class FunctionHelper {
    public static <T> Function<T, T> peek(Consumer<T> f) {
        return t -> {
            f.accept(t);
            return t;
        };
    }
}
