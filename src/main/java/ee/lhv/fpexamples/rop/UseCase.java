package ee.lhv.fpexamples.rop;

@FunctionalInterface
public interface UseCase<I, S, F> {
    Result<S, F> execute(I input);
}
