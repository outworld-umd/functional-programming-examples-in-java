package ee.lhv.fpexamples.purity;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntFunction;
import java.util.function.IntSupplier;

public class ImpureFunctionExample {

    private static int globalState;

    // impure (won't even compile)
//    private static final IntFunction<IntSupplier> GET_STATE_FUNCTION_LOCAL = start -> (() -> start++);

    // impure
    private static final IntFunction<IntSupplier> GET_STATE_FUNCTION_BAD = start -> {
        globalState = start;
        return () -> globalState++;
    };

    // impure
    private static final IntFunction<IntSupplier> GET_STATE_FUNCTION_BETTER = start -> {
        var localState = new AtomicInteger(start);
        return localState::getAndIncrement;
    };

    public static void main(String[] args) {
        var factory = GET_STATE_FUNCTION_BAD;

        var function1 = factory.apply(0);
        System.out.println(function1.getAsInt()); // 0
        System.out.println(function1.getAsInt()); // 1
        System.out.println(function1.getAsInt()); // 2

        var function2 = factory.apply(30);
        System.out.println(function2.getAsInt()); // 30
        System.out.println(function2.getAsInt()); // 31
        System.out.println(function2.getAsInt()); // 32

        System.out.println(function1.getAsInt()); // 3?
    }
}
