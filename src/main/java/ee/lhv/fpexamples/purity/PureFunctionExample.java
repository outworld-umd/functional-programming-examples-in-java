package ee.lhv.fpexamples.purity;

import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;

public class PureFunctionExample {
    private static final int CONSTANT = 20;

    // pure (some might argue)
    private static final IntUnaryOperator GET_NUMBER_FUNCTION_A = a -> CONSTANT * a;
    // pure
    private static final IntBinaryOperator GET_NUMBER_FUNCTION_B = (a, b) -> a * b;

    public static void main(String[] args) {
        // always 100, not modifying state
        System.out.println(GET_NUMBER_FUNCTION_A.applyAsInt(5));
        System.out.println(GET_NUMBER_FUNCTION_B.applyAsInt(20, 5));
    }
}
