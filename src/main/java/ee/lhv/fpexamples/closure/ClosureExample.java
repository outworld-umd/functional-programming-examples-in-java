package ee.lhv.fpexamples.closure;

public class ClosureExample {

    public static void main(String[] args) {
        var gravityCalculator = ScientificCalculatorFactory.getGravityCalculator(70);
        var result = gravityCalculator.calculate(50, 2);
        System.out.println(STR."Gravity between you and me: \{result} N");
    }


    private static class ScientificCalculatorFactory {
        private static final double G = 6.67430e-11;

        public static GravityCalculator getGravityCalculator(Integer mass) {
            return (otherMass, distance) -> G * ((mass * otherMass) / (distance * distance));
        }
    }

    @FunctionalInterface
    interface GravityCalculator {
        double calculate(double massKg, double distanceM);
    }
}
