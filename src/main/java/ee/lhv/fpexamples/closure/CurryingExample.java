package ee.lhv.fpexamples.closure;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class CurryingExample {

    private static final Function<BigDecimal, Function<BigDecimal, Function<BigDecimal, BigDecimal>>> PRICE_CALCULATOR =
            exchangeRate -> taxRate -> priceForeign -> priceForeign
                    .add(priceForeign.multiply(taxRate).divide(BigDecimal.valueOf(100), RoundingMode.HALF_UP))
                    .multiply(exchangeRate)
                    .setScale(2, RoundingMode.HALF_UP);

    public static void main(String[] args) {
        var priceCalculatorUsd = PRICE_CALCULATOR.apply(BigDecimal.valueOf(0.93));

        var priceCalculatorUsdOldTax = priceCalculatorUsd.apply(new BigDecimal("20"));
        var priceCalculatorUsdNewTax = priceCalculatorUsd.apply(new BigDecimal("22"));

        Supplier<Stream<BigDecimal>> usdPrices = () -> Stream.of(50.0, 94.99, 8.69).map(BigDecimal::valueOf);
        var pricesUsdAndOldTax = usdPrices.get().map(priceCalculatorUsdOldTax).toList();
        System.out.println(STR."Prices (USD) with 20% tax: \{pricesUsdAndOldTax}");
        var pricesUsdAndNewTax = usdPrices.get().map(priceCalculatorUsdNewTax).toList();
        System.out.println(STR."Prices (USD) with 22% tax: \{pricesUsdAndNewTax}");


        var priceCalculatorSek = PRICE_CALCULATOR.apply(BigDecimal.valueOf(0.086));

        var priceCalculatorSekNewTax = priceCalculatorSek.apply(new BigDecimal("24"));

        Supplier<Stream<BigDecimal>> sekPrices = () -> Stream.of(250.0, 954.99, 18.69).map(BigDecimal::valueOf);
        var pricesSekAndTax = sekPrices.get().map(priceCalculatorSekNewTax).toList();
        System.out.println(STR."Prices (SEK) with 24% tax: \{pricesSekAndTax}");
    }
}
