package ee.lhv.fpexamples.matching;

// type Expression =
// | Const of double
// | Add of Expression * Expression
// | Subtract of Expression * Expression
// | Multiply of Expression * Expression
// | Divide of Expression * Expression
public sealed interface Expression {
    record Const(Double number) implements Expression {}
    record Add(Expression x, Expression y) implements Expression {}
    record Multiply(Expression x, Expression y) implements Expression {}
    record Subtract(Expression x, Expression y) implements Expression {}
    record Divide(Expression x, Expression y) implements Expression {}
}
