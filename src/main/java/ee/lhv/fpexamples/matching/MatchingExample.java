package ee.lhv.fpexamples.matching;

public class MatchingExample {

    public static void main(String[] args) {
        // (2 * (-24 + 28)) + (10 / (13 - 11))
        var expression = new Expression.Add(
                new Expression.Multiply(
                        new Expression.Const(2.0),
                        new Expression.Add(
                                new Expression.Const(-24.0),
                                new Expression.Const(28.0))),
                new Expression.Divide(
                        new Expression.Const(10.0),
                        new Expression.Subtract(
                                new Expression.Const(13.0),
                                new Expression.Const(11.0)))
        );

        var result = evaluate(expression);
        System.out.println(result); // 13
    }

    public static double evaluate(Expression expr) {
        return switch (expr) {
            case Expression.Const(Double num) -> num;
            case Expression.Add(Expression x, Expression y) -> evaluate(x) + evaluate(y);
            case Expression.Multiply(Expression x, Expression y) -> evaluate(x) * evaluate(y);
            case Expression.Divide(Expression x, Expression y) -> evaluate(x) / evaluate(y);
            case Expression.Subtract(Expression x, Expression y) -> evaluate(x) - evaluate(y);
        };
    }
}
