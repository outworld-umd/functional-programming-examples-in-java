package ee.lhv.fpexamples.laziness;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class MemoizationExample {

    public static void main(String[] args) {
        Supplier<Stream<Integer>> stream = () -> Stream.of(1, 2, 3, 2, 3, 1);

        System.out.println("Starting stream!");
        var resultBad = stream.get()
                .map(ComplexOperationUtility::complexOperation)
                .toList();
        System.out.println(resultBad);

        System.out.println("Starting stream with cached op!");
        var resultCached = stream.get()
                .map(cached(ComplexOperationUtility::complexOperation))
                .toList();
        System.out.println(resultCached);
    }

    public static <T, U> Function<T, U> cached(Function<T, U> func) {
        var cache = new ConcurrentHashMap<T, U>();
        return input -> cache.computeIfAbsent(input, func);
    }
}
