package ee.lhv.fpexamples.laziness;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import static ee.lhv.fpexamples.laziness.ComplexOperationUtility.complexOperation;

public class LazinessExample {

    public static void main(String[] args) {
//        var lazyValue = Lazy.of(() -> complexOperation(123));
        var lazyValue = lazy(() -> complexOperation(123));

        Runnable run = () -> {
            System.out.println("Starting runnable!");
            System.out.println(lazyValue.get());
            System.out.println(lazyValue.get());
        };

        new Thread(run).start();
        new Thread(run).start();
        new Thread(run).start();

        run.run();
    }

    public static <T> Supplier<T> lazy(Supplier<T> supplier) {
        var cache = new ConcurrentHashMap<Integer, T>(1);
        return () -> cache.computeIfAbsent(0, _ -> supplier.get());
    }

}
