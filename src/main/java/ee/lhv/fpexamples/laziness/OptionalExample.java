package ee.lhv.fpexamples.laziness;

import lombok.extern.java.Log;

import java.util.Optional;

import static ee.lhv.fpexamples.laziness.ComplexOperationUtility.complexOperation;

@Log
public class OptionalExample {

    public static void main(String[] args) {
        var data = new DataStructure("Some data", null);

        System.out.println("[Bad approach]");
        var resultBad = Optional.of(data)
                .map(DataStructure::num)
                .orElse(complexOperation(123));
        System.out.println(resultBad);

        System.out.println("[Nice approach]");
        var resultOk = Optional.of(data)
                .map(DataStructure::num)
                .orElseGet(() -> complexOperation(456));
        System.out.println(resultOk);
    }

    record DataStructure(String text, Integer num) {
    }
}