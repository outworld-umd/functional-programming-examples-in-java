package ee.lhv.fpexamples.laziness;

import lombok.SneakyThrows;

public class ComplexOperationUtility {

    @SneakyThrows
    public static int complexOperation(Integer num) {
        System.out.println(STR."Performing complex operation for \{num}!");
        Thread.sleep(1000);
        return num + 10;
    }
}
