package ee.lhv.fpexamples.composition;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class CompositionPipeExample {

    private static final Function<Stream<Product>, BigDecimal> getTotalSum =
            products -> products.map(Product::price).reduce(BigDecimal.ZERO, BigDecimal::add);
    private static final UnaryOperator<BigDecimal> applyDeliveryFee =
            total -> total.doubleValue() < 50 ? total.add(new BigDecimal("2.50")) : total;
    private static final Function<String, UnaryOperator<Stream<Product>>> filterCategories = category ->
            products -> products.filter(product -> category.equals(product.category()));
    private static final Function<BigDecimal, UnaryOperator<BigDecimal>> applyDiscount = discount ->
            total -> total.subtract(discount).max(BigDecimal.ZERO);
    private static final Function<BigDecimal, Integer> round =
            value -> value.intValue() + 1;


    // Composed functions
    public static final Function<String, Function<Collection<Product>, Integer>> getRoundedDeliverySumByCategory = category ->
            filterCategories.apply(category)
                    .andThen(getTotalSum)
                    .andThen(applyDeliveryFee)
                    .andThen(round)
                    .compose(Collection::stream);

    public static final BiFunction<String, BigDecimal, Function<Collection<Product>, Integer>> getRoundedAndDiscountedSum = (category, discount) ->
            filterCategories.apply(category)
                    .andThen(getTotalSum)
                    .andThen(applyDiscount.apply(discount))
                    .andThen(round)
                    .compose(Collection::stream);


    public static void main(String[] args) {
        var products = List.of(
                new Product("IT", "Apple Computer", BigDecimal.valueOf(500)),
                new Product("Food", "Apple", BigDecimal.valueOf(5.20)),
                new Product("Food", "Banana", BigDecimal.valueOf(3.40)),
                new Product("IT", "Lenovo", BigDecimal.valueOf(49.90))
        );

        var result1 = getRoundedDeliverySumByCategory.apply("Food").apply(products);
        // 12 = (5.20 + 3.40) + 2.50 >> ceil
        System.out.println(result1);

        var result3 = getRoundedAndDiscountedSum.apply("IT", BigDecimal.TEN).apply(products);
        // 540 = (500.00 + 49.90) - 10.00 >> ceil
        System.out.println(result3);

    }

    public record Product(String category, String name, BigDecimal price) {
    }
}
